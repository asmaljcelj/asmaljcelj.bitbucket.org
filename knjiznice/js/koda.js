
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var sessionId = null;



/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";
  // TODO: Potrebno implementirati
  sessionId = getSessionId();
  var ime = "";
  var priimek = "";
  var datumRojstva = "";
  var cas = "";
  var teza = "";
  var temperatura = "";
  var krvniTlakS = "";
  var krvniTlakD = "";
  switch (stPacienta) {
    case 1: ime = "Andrej";
            priimek = "Tekač";
            datumRojstva = "1980-10-15T14:00Z";
            cas = "2016-05-04T12:00Z";
            teza = "90";
            temperatura = "36";
            krvniTlakS = "125";
            krvniTlakD = "83";
            break;
    case 2: ime = "Alenka";
            priimek = "Upokojenka";
            datumRojstva = "1951-05-13T08:15Z";
            cas = "2018-05-26T19:30Z";
            teza = "50";
            temperatura = "37.50";
            krvniTlakS = "105";
            krvniTlakD = "65";
            break;
    case 3: ime = "Maja";
            priimek = "Otrok";
            datumRojstva = "2005-11-06T17:00Z";
            cas = "2016-05-04T12:00Z";
            teza = "35";
            temperatura = "35.90";
            krvniTlakS = "110";
            krvniTlakD = "60";
            break;
  }
  $.ajaxSetup({
    headers: {"Ehr-Session": sessionId}
  });
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function(data) {
      ehrId = data.ehrId;
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
          var string = ime.concat(",", priimek, ",", datumRojstva);
          var string2 = ehrId.concat("|", cas, "|", teza, "|", temperatura, "|", krvniTlakS, "|", krvniTlakD);
          $("#preberiPredlogoBolnika").append("<option value="+string+">"+ime+" "+priimek+"</option");
          $("#preberiObstojeciVitalniZnak").append("<option value="+string2+">"+ime+" "+priimek+"</option>");
          $("#preberiEhrIdZaVitalneZnake").append("<option value="+ehrId+">"+ime+" "+priimek+"</option>");
        }
      });
      for (var i = 0; i < 10; i++) {
        var datumInUra = "";
	      var telesnaTeza = "";
	      var telesnaTemperatura = "";
	      var sistolicniKrvniTlak = "";
	      var diastolicniKrvniTlak = "";
	      switch(stPacienta) {
	        case 1:
	          telesnaTeza = Math.floor(Math.random()*3)+82;
	          telesnaTemperatura = Math.round(((Math.random()*1.5)+36)*10)/10;
	          sistolicniKrvniTlak = Math.floor(Math.random()*14)+118;
	          diastolicniKrvniTlak = Math.floor(Math.random()*7)+80;
	          break;
	        case 2:
	          telesnaTeza = Math.floor(Math.random()*4)+65;
	          telesnaTemperatura = Math.round(((Math.random()*2)+36)*10)/10;
	          sistolicniKrvniTlak = Math.floor(Math.random()*14)+138;
	          diastolicniKrvniTlak = Math.floor(Math.random()*7)+88;
	          break;
	        case 3:
	          telesnaTeza = Math.floor(Math.random()*5)+40;
	          telesnaTemperatura = Math.round(((Math.random()*1.5)+37)*10)/10;
	          sistolicniKrvniTlak = Math.floor(Math.random()*10)+105;
	          diastolicniKrvniTlak = Math.floor(Math.random()*10)+55;
	          break;
	      }
	    
	    
         $.ajaxSetup({
		        headers: {"Ehr-Session": sessionId}
		      });
		     var podatki = {
		        "ctx/language": "en",
		        "ctx/territory": "SI",
		        "ctx/time": datumInUra,
		        "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	    "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		        "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		        "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		      };
		      var parametriZahteve = {
		        ehrId: ehrId,
		        templateId: 'Vital Signs',
		        format: 'FLAT',
		      };
		      $.ajax({
		        url: baseUrl + "/composition?" + $.param(parametriZahteve),
		        type: 'POST',
		        contentType: 'application/json',
		        data: JSON.stringify(podatki),
		        success: function (res) {
		        }
		      });
      }
    }
  });
  alert("kreiran/a nova oseba: "+ime+" "+priimek);
  return ehrId;
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		                var string = ime.concat(",", priimek, ",", datumRojstva);
		                $("#preberiObstojeciVitalniZnak").append("<option value="+ehrId+">"+ime+" "+priimek+"</option>");
                    $("#preberiPredlogoBolnika").append("<option value="+string+">"+ime+" "+priimek+"</option");
                    $("#preberiEhrIdZaVitalneZnake").append("<option value="+ehrId+">"+ime+" "+priimek+"</option>");
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}
var temperatura = [];
var teza = [];
var tlak = [];

var povprecnaTemp = 0;
var povprecniTlakD = 0;
var povprecniTlakS = 0;

function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();
	$("#tekst").html('');
	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "telesna temperatura") {
					$.ajax({
  					  url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th></th><th></th><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
						        for (var i = 0; i < res.length; i++) {
						        		povprecnaTemp += res[i].temperature;
						        		var meritev = i+1;
						            temperatura[i] = [i+1, res[i].temperature];
						            var string = "";
						            if (i > 0) {
						            	var razlika = Math.round(Math.abs(res[i].temperature - res[i-1].temperature)*10)/10;
						            	string = meritev+". meritev, opravljena dne "+res[i-1].time.split("T")[0]+", se razlikuje od prejšnje meritve, ki je bila izmerjena "+res[i].time.split("T")[0]+" za "+razlika+" stopinj/o Celzija.";
						            } else {
						            	string = meritev+". meritev, opravljena dne "+res[i].time.split("T")[0]+".";
						            }
						            results += " \
						            		<tr> \
						            				<td><button type='button' class='btn btn-info' data-toggle='collapse' data-target='#collapse"+i+"'>Podrobnosti</button><td> \
						            				<td class='text-left'>" + res[i].time.split(".")[0] + "</td> \
                          			<td class='text-right'>" + res[i].temperature + " " + res[i].unit + "</td> \
                          	</tr> \
                          	<tr id='collapse"+i+"' class='accordion-body collapse'> \
                          		<td colspan='4'>"+string+"</td> \
                          	</tr>";
						        }
						        povprecnaTemp = povprecnaTemp/(res.length+1);
						        if (povprecnaTemp > 37) {
						        	alert("Vaše najnovejše meritve kažejo, da imate vročino! Preberite si kratka navodila, kako ravnati naprej!");
						        	vrocina();
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
						        narisiGrafTemperature();
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    	
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					
				} else if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th></th><th></th><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i = 0; i < res.length; i++) {
						        		var meritev = i+1;
						        		var string = "";
						            if (i > 0) {
						            	var razlika = Math.abs(res[i].weight - res[i-1].weight);
						            	string = meritev+". meritev, opravljena dne "+res[i-1].time.split("T")[0]+", se razlikuje od prejšnje meritve, ki je bila izmerjena "+res[i].time.split("T")[0]+" za "+razlika+" kg."
						            } else {
						            	string = meritev+". meritev, opravljena dne "+res[i].time.split("T")[0]+".";
						            }
						        		teza[i] = [i+1, res[i].weight];
						            results += " \
						            	<tr> \
						            		<td><button type='button' class='btn btn-info' data-toggle='collapse' data-target='#collapse"+i+"'>Podrobnosti</button><td> \
						            		<td class='text-left'>" + res[i].time.split(".")[0] +"</td> \
						            		<td class='text-right'>" + res[i].weight + " " 	+res[i].unit + "</td> \
						            	</tr> \
						            	<tr id='collapse"+i+"' class='accordion-body collapse'> \
                          		<td colspan='4'>"+string+"</td> \
                          <tr>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
						      	narisiGrafTeze();
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "krvni tlak") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var results = "<table class='table table-striped table-hover'>" +
                  "<tr><th></th><th></th><th>Datum in ura</th><th class='text-right'>" +
                  "Krvni tlak</th></tr>";
					    	if (res) {
						        for (var i = 0; i < res.length; i++) {
						        		povprecniTlakD += res[i].diastolic;
						        		povprecniTlakS += res[i].systolic;
						        		var meritev = i+1;
						        		var string = "";
						            if (i > 0) {
						            	var razlikaS = Math.abs(res[i].systolic - res[i-1].systolic);
						            	var razlikaD = Math.abs(res[i].diastolic - res[i-1].diastolic);
						            	string = meritev+". meritev, opravljena dne "+res[i-1].time.split("T")[0]+", se razlikuje od prejšnje meritve, ki je bila izmerjena "+res[i].time.split("T")[0]+", za "+razlikaS+" mm Hg v sistoličnem tlaku in za "+razlikaD+" mmHg v diastoličnem krvnem tlaku.";
						            } else {
						            	string = meritev+". meritev, opravljena dne "+res[i].time.split("T")[0]+".";
						            }
						        		tlak[i] = [res[i].systolic, res[i].diastolic];
						            results += " \
						            	<tr> \
						            		<td><button type='button' class='btn btn-info' data-toggle='collapse' data-target='#collapse"+i+"'>Podrobnosti</button><td> \
						            		<td class='text-left'>" + res[i].time.split(".")[0] + "</td> \
						            		<td class='text-right'>" + res[i].systolic + "/" + res[i].diastolic + " mmHg</td> \
						            	</tr> \
						            	<tr id='collapse"+i+"' class='accordion-body collapse'> \
                          		<td colspan='4'>"+string+"</td> \
                          <tr>";
						        }
						        povprecniTlakD /= res.length;
						        povprecniTlakS /= res.length;
						        if (povprecniTlakS > 135 && povprecniTlakD > 88) {
						        	alert("Glede na najnovejše meritve imate povišan krvni tlak! Spodaj si preberite, kako se krvni tlak ohranja. Če se težave nadaljujejo, obiščite zdravnika!");
						        	tlakWiki();
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
						        narisiGrafKrvnegaTlaka();
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}

					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

function vrocina() {
	$("#tekst").html('');
	$.getJSON("https://en.wikipedia.org/w/api.php?action=parse&format=json&section=14&prop=text&page=Fever&callback=?", function(data) {
		var HTMLKoda = data.parse.text['*'];
		$("#tekst").append(HTMLKoda.split("</span></span></h2>")[1].split("<h3><span class=")[0]);
	});
}

function tlakWiki() {
	$("#tekst").html('');
	$.getJSON("https://sl.wikipedia.org/w/api.php?action=parse&format=json&section=1&prop=text&page=Krvni_tlak&callback=?", function(data) {
		var HTMLKoda = data.parse.text['*'];
		console.log(data.parse.text['*']);
		$("#tekst").append(HTMLKoda.split("</span></span></h2>")[1].split("<p>Obstaja tudi nekaj hormonov, ki vplivajo na krvni tlak. Ti so:</p>")[0]);
	});
}

function narisiGrafTemperature (podatki) {
	var vrednosti = new google.visualization.DataTable;
	vrednosti.addColumn('number');
	vrednosti.addColumn('number');
	vrednosti.addRows(temperatura);
	var options = {
		hAxis: {
        title: 'Zaporedna meritev', minValue: 1, maxValue: 10
     },
    vAxis: {
       title: 'Telesna temperatura [C]', minValue: 30, maxValue: 40
     },
    legend: 'none',
		title: 'Telesna temperatura',
		width: 500,
		height: 350
	};
	var graf = new google.visualization.LineChart(document.getElementById('chart_div'));
	graf.draw(vrednosti, options);
}

function narisiGrafTeze (podatki) {
	var vrednosti = new google.visualization.DataTable;
	vrednosti.addColumn('number');
	vrednosti.addColumn('number');
	vrednosti.addRows(teza);
	var options = {
		hAxis: {
        title: 'Zaporedna meritev', minValue: 1, maxValue: 10
     },
    vAxis: {
       title: 'Telesna teža [kg]', minValue: 50, maxValue: 100
     },
    legend: 'none',
		title: 'Telesna teza',
		width: 500,
		height: 350
	};
	var graf = new google.visualization.LineChart(document.getElementById('chart_div'));
	graf.draw(vrednosti, options);
}

function narisiGrafKrvnegaTlaka (podatki) {
	var vrednosti = new google.visualization.DataTable;
	vrednosti.addColumn('number');
	vrednosti.addColumn('number');
	vrednosti.addRows(tlak);
	var options = {
		hAxis: {
        title: 'Sistolični tlak [mm Hg]', minValue: 0, maxValue: 140
     },
    vAxis: {
       title: 'Diastolični tlak [mm Hg]', minValue: 0, maxValue: 100
     },
    legend: 'none',
		title: 'Krvni tlak',
		width: 500,
		height: 350
	};
	var graf = new google.visualization.ScatterChart(document.getElementById('chart_div'));
	graf.draw(vrednosti, options);
}


$(document).ready(function() {
	
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2].split("T")[0]);
  });
  
  $('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
	});
	
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
});